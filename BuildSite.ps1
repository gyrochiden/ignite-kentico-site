[cmdletbinding()]
param(
    [string] $SolutionPath = "",
    [string] $IgnitionPath = "",
    [string] $BuildTarget = ""
)

if ((!$SolutionPath) -or ($SolutionPath -and !(Test-Path $SolutionPath))) {
    $SolutionPath = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
    if (!(Test-Path $SolutionPath)) {
        Throw "Solution path is not found! Please make sure you are executing this command from the correct folder! or check your -SolutionPath parameter"
    }
}

if ((!$IgnitionPath) -or ($IgnitionPath -and !(Test-Path $IgnitionPath))) {
    $IgnitionPath = (Split-Path $MyInvocation.MyCommand.Path -Parent)
    if (!(Test-Path $IgnitionPath)) {
        Throw "Ignition path is not found! Please make sure you are executing this command from the correct folder! or check your -IgnitionPath parameter"
    }
}


# function Get-MSBuildPath ([switch] $amd64) {
#     Write-Host "Finding MSBuild...."
#     $vsInstallPath = (Get-VsSetupInstance).InstallationPath    
#     $msBuildPaths = Get-ChildItem -Path $vsInstallPath -Recurse | Where-Object { $_.Name -ieq 'MsBuild.exe' }   
#     $msBuildPathsSorted = $msBuildPaths | Sort-Object -Property FullName -Descending

#     $msBuildPath = $null 
#     if ($amd64)
#     {
#         $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ieq 'amd64' } | Select-Object -ExpandProperty FullName -First 1
#     }
#     else
#     {
#         $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ine 'amd64' } | Select-Object -ExpandProperty FullName -First 1        
#     }

#     $msBuildPath
    
# }

class MSBuildFinder {
    [string]GetMsBuildPath($amd64) {
        $vsInstallPath = (Get-VsSetupInstance).InstallationPath    
        $msBuildPaths = Get-ChildItem -Path $vsInstallPath -Recurse | Where-Object { $_.Name -ieq 'MsBuild.exe' }   
        $msBuildPathsSorted = $msBuildPaths | Sort-Object -Property FullName -Descending
    
        $msBuildPath = $null 
        if ($amd64)
        {
            $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ieq 'amd64' } | Select-Object -ExpandProperty FullName -First 1
        }
        else
        {
            $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ine 'amd64' } | Select-Object -ExpandProperty FullName -First 1        
        }
    
        return $msBuildPath
    }
}

function doBuild() {
    begin {
        Set-Location $SolutionPath
        $solution = '*.sln'
        $solutionMatches = @(Get-ChildItem $solution -ErrorAction SilentlyContinue)
    }

    process {
        if ($solutionMatches.Length -ne 1) {
            Write-Warning "Expected 1 solution match but found $($solutionMatches.Length). Refine solution path."
            return
        }
        
        $sln = $solutionMatches[0]
        $msBuildFinder = New-Object -TypeName MSBuildFinder
        $msBuild = $msBuildFinder.GetMsBuildPath($FALSE)
        
        &$msBuild -m $sln.FullName /property:GenerateFullPaths=true /t:$($BuildTarget)
    }
}

doBuild