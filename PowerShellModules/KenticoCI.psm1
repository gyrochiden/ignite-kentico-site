function Restore-KenticoCIData {
    param (
        [string] $SolutionPath,
        [string] $IgnitionPath
    )
    
    begin {
        Set-Location $SolutionPath
        $config = (Get-Content .\ignition.json) | ConvertFrom-Json    
        $BIN_DIR = Join-Path $SolutionPath "CMS\bin"
        $CI_TOOL = Join-Path $BIN_DIR "ContinuousIntegration.exe"
    }


    process {
    
        Import-Module $IgnitionPath\PowerShellModules\SQLLocalDBUtils.psm1
        $ldb = $config.dev.databaseInstance
        Write-Host "Ensuring (localdb)\$ldb is started"
        Start-LocalDb($config.dev.databaseInstance) | Out-Null
        Write-Host "Running Kentico CI Process..."
        Invoke-Expression "&`"$CI_TOOL`" -r"
    }
}

Export-ModuleMember -function Restore-KenticoCIData