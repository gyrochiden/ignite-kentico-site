#Re-maps user shared drives to administrator user so they can use them.
$elevated = (([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]::Administrator))
if( $elevated ) {
    net use | ?{ $_ -match ":\s+\\\\"  -and !$_.StartsWith("Unavailable") } | %{
        $tokens = $_.split(":")
        $psdrivename = $tokens[0][$tokens[0].length-1]
        $path = $tokens[1].trim().split(" ")[0].trim()

        if( !(get-psdrive | ?{ $_.Name -eq $psdrivename } )) {
            #write-host ( "Restoring mapped drive {0}: {1}" -f $psdrivename, $path )
            #new-psdrive $psdrivename FileSystem $path
            
              $nwrk=new-object -com Wscript.Network
              Write-Host "Mapping $($psdrivename+':') to $path and persist=$true"
              try{
                   $nwrk.MapNetworkDrive($($psdrivename+':'),$path)     
                   Write-Verbose "Mapping successful."
              }
              catch{
                   Write-Host "Mapping failed!"
              }
        }
    }
}
