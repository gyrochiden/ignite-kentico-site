﻿function Install-RequiredTools {

    begin {    
        $UTILS_DIR = ("$env:LOCALAPPDATA\.gyro\igniteSiteUtils\")
        $NUGET = Join-Path $UTILS_DIR "nuget.exe"
        $NUGET_DOWNLOAD_URL = "https://dist.nuget.org/win-x86-commandline/latest/nuget.exe"
    }

    process {
    
        if(!$PSScriptRoot){
            $PSScriptRoot = Split-Path $MyInvocation.MyCommand.Path -Parent
        }

        if ((Test-Path $PSScriptRoot) -and !(Test-Path $UTILS_DIR)) {
            Write-Host "Creating utils directory..."
            New-Item -Path $UTILS_DIR -Type directory | out-null
        }


        if (!(Test-Path $NUGET)) {
            Write-Host "Downloading NuGet..."
            try {
                (New-Object System.Net.WebClient).DownloadFile($NUGET_DOWNLOAD_URL, $NUGET)
            } catch {
                Throw "NuGet Download Failed!"
            }
        }

        Write-Host "Installing / Updating required packages and utilites using NuGet..."
        Invoke-Expression "&`"$NUGET`" install $PSScriptRoot\..\packages.config -ExcludeVersion -Verbosity quiet -OutputDirectory `"$UTILS_DIR`""

        if (!(Get-Module -ListAvailable -Name SqlServer)) {
            Write-Host "Installing SqlServer Module for PowerShell..."
            Install-Module -Name SqlServer -AllowClobber
        } 
        
        if (!(Get-Module -ListAvailable -Name VSSetup)) {
            Write-Host "Installing VSSetup..."
            Install-Module -Name VSSetup -RequiredVersion 2.2.5
        }

        if ($LASTEXITCODE -ne 0) {
            Throw "Installing required packages and utilites failed!"
        }

    }

    end {
    
        return $UTILS_DIR
    
    }

}

Export-ModuleMember -function Install-RequiredTools