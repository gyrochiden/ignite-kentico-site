function Install-Site {
    param(
    [string] $SolutionPath,
    [string] $IgnitionPath,
    [string] $UtilsPath
    )

    begin {
        Set-Location $SolutionPath
        Import-Module -Name SqlServer -Force
        $config = (Get-Content .\ignition.json) | ConvertFrom-Json
        $solution_built = $FALSE
    }

    process {

            if($config.dev.createBinFolder) {
                Create-Bin-Folder
            }

            if($config.dev.createDatabase) {
                Download-Database
                Create-Database
            }

            Update-IISSettings
            Update-FolderPermissions
            
            Update-HostFile

            
            $build_yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Builds the solution"
            $build_no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Continues without building the solution"
            $build_options = [System.Management.Automation.Host.ChoiceDescription[]]($build_yes, $build_no)
            $build_result = Show-Menu 'Build Solution?' 'Would you like to attempt to build the solution?' $build_options

            switch ($build_result)
            {
                0 {
                    try {
                        Invoke-MSBuild '*.sln'
                        $solution_built = $TRUE
                    } catch {
                        Write-Warning("Building Solution Failed!!")
                    }
                }
            }

            if($solution_built) {
                Restore-CIData
            }
    }

    end {
        Write-Host("")
        Write-Host("Site Ignition Completed!") -ForegroundColor Green
        Write-Host("")
        if(!($solution_built)) {            
            Write-Host("WARNING: Site will not work until the solution has been built") -ForegroundColor Yellow
            Write-Host("you need to open the solution file and perform a build!") -ForegroundColor Yellow
            Write-Host("")
            Write-Host("NOTE: Some versions of Visual Studio struggle with package restore for Roslyn") -ForegroundColor Magenta
            Write-Host("the best way around this is to build the solution, and then close Visual Studio") -ForegroundColor Magenta
            Write-Host("then re-open the solution and run a 'Build > Clean', then rebuild the solution") -ForegroundColor Magenta
            Write-Host("subsequent builds should work as normal!") -ForegroundColor Magenta
            Write-Host("")
        }
        Write-Host("If this site is on a shared drive you may need to update IIS to point to the proper UNC path!") -ForegroundColor Cyan
        Write-Host("")        
        Write-Host("You may now close this window.....") -ForegroundColor Green
    }
}

function Create-Bin-Folder {

    begin{
        Write-Host "Creating CMS\Bin Folder..."

    }

    process{
        New-Item .\CMS\Bin -ItemType Directory -Force | Out-Null
        Write-Host "Copying Kentico Base Library to CMS\Bin Folder..."
        Copy-Item .\Lib\*.dll CMS\Bin -Force | Out-Null
        Copy-Item .\Lib\*.config CMS\Bin -Force | Out-Null
        Copy-Item .\Lib\*.exe CMS\Bin -Force | Out-Null
    }

}

function Download-Database {
    #Download Base DB File
    Write-Host "Downloading the base database image..."
    $url = $config.dev.baseDBUrl
    $bacPac = $config.dev.bacpacFile
    $db_output_path = "$IgnitionPath\DB\$bacPac"
    (New-Object System.Net.WebClient).DownloadFile($url, $db_output_path)
}

function Create-Database {

    begin {
        Import-Module $IgnitionPath\PowerShellModules\SQLLocalDBUtils.psm1
        $SQL_SERVER = $config.dev.databaseServer + $config.dev.databaseInstance
        $SQL_CONNECTION_STRING = "Data Source=$SQL_SERVER;Initial Catalog=master;Connection Timeout=0;Integrated Security=True;"
        $BACPAC_FILE = Join-Path "$IgnitionPath\DB\" $config.dev.bacpacFile
        $DAC_FX_PATH = Join-Path $UtilsPath "\Microsoft.SqlServer.DacFx.x64\lib\net46\Microsoft.SqlServer.Dac.dll"
        $dbUsername = $config.dev.databaseUser
        $dbPassword = $config.dev.databasePassword
        $database = $config.dev.databaseName
    }

    process {
        Write-Host Creating SQL localdb instance $config.dev.databaseInstance
        Add-LocalDb($config.dev.databaseInstance) | Out-Null

        if ((Test-Path $DAC_FX_PATH) -eq $false) {
            Throw 'No usable version of DacFx found.'
        }
        else {
            try {
                Add-Type -Path $DAC_FX_PATH
            }
            catch {
                Throw 'No usable version of DacFx found.'
            }
        }

        $bacPacRestored = $FALSE
        try {
            Write-Host Importing $config.dev.bacpacFile to $SQL_SERVER
            $DacServices = New-Object Microsoft.SqlServer.Dac.DacServices $SQL_CONNECTION_STRING
            $BacPackage = [Microsoft.SqlServer.Dac.BacPackage]::Load($BACPAC_FILE)
            $DacServices.ImportBacpac($BacPackage, $database)
            $bacPacRestored = $TRUE
        } catch {
            #Write-Warning "Database Import Failed! Database has probably already been imported. If you need to re-import the initial database it must be dropped using SQL Management Studio first."
        }

        if(!($bacPacRestored)) {
            $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Drops the database and restores the base database."
            $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Keeps the current database and continues."
            $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
            $result = Show-Menu "Database $database already exists!" 'Do you wish to delete the existing database and restore the base database?' $options

            switch ($result)
            {
                0 {
                    Write-Host("Dropping $database database...")
                    $drop_sql="
                        USE [master]
                        GO

                        DROP DATABASE [$database]
                        GO"

                    Invoke-Sqlcmd -Server $SQL_SERVER -Query $drop_sql | Out-Null
                    try {
                        Write-Host Importing $config.dev.bacpacFile to $SQL_SERVER
                        $DacServices = New-Object Microsoft.SqlServer.Dac.DacServices $SQL_CONNECTION_STRING
                        $BacPackage = [Microsoft.SqlServer.Dac.BacPackage]::Load($BACPAC_FILE)
                        $DacServices.ImportBacpac($BacPackage, $database)
                        $bacPacRestored = $TRUE
                    } catch {
                        Write-Warning "Database DROP and re-import Failed! If you need to re-import the initial database it must be dropped/deleted using SQL Management Studio first."
                    }

                }
                1 {"Existing database has been retained"}
            }
        }

        Write-Host("Fixing login for user $dbUsername")
        $sql="
        USE [master]
        GO

        if not exists (select loginname from master.dbo.syslogins where loginname='$dbUsername')
            CREATE LOGIN [$dbUsername] WITH PASSWORD='$dbPassword'
        GO

        ALTER LOGIN [$dbUsername] WITH PASSWORD = '$dbPassword';
        GO

        use [$database]
        GO

        IF EXISTS (SELECT name FROM [sys].[database_principals] WHERE  name = N'$dbUsername')
            DROP USER [$dbUsername]
        GO

        CREATE USER [$dbUsername] FOR LOGIN [$dbUsername] WITH DEFAULT_SCHEMA=[dbo]
        GO

        ALTER ROLE [db_owner] ADD MEMBER [$dbUsername]
        GO"

        Invoke-Sqlcmd -Server $SQL_SERVER -Query $sql | Out-Null
        Enable-LocalDbSharing($config.dev.databaseInstance) | Out-Null
        Stop-LocalDb($config.dev.databaseInstance) | Out-Null
        Start-LocalDb($config.dev.databaseInstance) | Out-Null

        Write-Host("Extending auto-close/shutdown timeout for localdb instance")
        $autoShutdownExtendSQL="
        sp_configure 'show advanced options', 1;
        RECONFIGURE;
        GO
        sp_configure 'user instance timeout', 65535;
        GO"

        Invoke-Sqlcmd -Server $SQL_SERVER -Query $autoShutdownExtendSQL | Out-Null
        Stop-LocalDb($config.dev.databaseInstance) | Out-Null
        Start-LocalDb($config.dev.databaseInstance) | Out-Null
    }

    end {

        Write-Host Initial Database Setup Complete!

    }

}

function Update-FolderPermissions {
    begin { 
        Write-Host "Updating folder permissions for IIS"
        $iis = Get-IISServerManager
        $siteName = $config.dev.siteName
        $appPool = $iis.ApplicationPools[$siteName]
        $appPoolSid = $appPool.RawAttributes["applicationPoolSid"]
        $identifier = New-Object System.Security.Principal.SecurityIdentifier $appPoolSid
        $appPoolUser = $identifier.Translate([System.Security.Principal.NTAccount])
    }

	process {
        
        
        $Acl = Get-Acl ".\CMS"
		$permissions = "IIS_IUSRS", 'Read,Modify', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
        $permissions_anon = "IUSR", 'Read', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
        $permissions_apppool = $appPoolUser, 'FullControl', 'ContainerInherit,ObjectInherit', 'None', 'Allow'
		$rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions
        $rule_apppool = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions_apppool
        $rule_anon = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $permissions_anon        
        $Acl.SetAccessRule($rule)
        $Acl.SetAccessRule($rule_apppool)
		$Acl.SetAccessRule($rule_anon)
		Set-Acl ".\" $Acl
    }

	end { Write-Host "Updated folder permissions" }
}

function Update-IISSettings {
    begin{
        Import-Module IISAdministration
        $iis = Get-IISServerManager
        $siteName = $config.dev.siteName
        $bindings = $config.dev.bindings
		$logicalDisk = (Get-CimInstance Win32_LogicalDisk -filter "DriveType = 4 AND DeviceID = '$((Get-Location).Drive.Name):'").ProviderName
		$iisPath =  "$SolutionPath\CMS"
		if($logicalDisk) {
			$uncPath = (Get-Location).Path.Replace((Split-Path -qualifier (Get-Location).Path), $logicalDisk)
			$iisPath = "$uncPath\CMS"
		}		
    }

    process {
        Write-Host "Setting up IIS..."
        $defaultSite = $iis.Sites["Default Web Site"]
        if($defaultSite) {
            $binding = $defaultSite.Bindings[0]
            if($binding) {
                $binding.BindingInformation = "*:80:localhost"
            }
            $iis.CommitChanges()
        }

        Write-Host Creating IIS App Pool... [$siteName]
        if($null -ne $iis.ApplicationPools -And  $null -ne $iis.ApplicationPools[$siteName]) {
            $iis.ApplicationPools.Remove($iis.ApplicationPools[$siteName])
        }
        $pool = $iis.ApplicationPools.Add($siteName)
        $pool.ManagedPipelineMode = "Integrated"
        $pool.ManagedRuntimeVersion = "v4.0"
        $pool.Enable32BitAppOnWin64 = $false
        $pool.AutoStart = $true
        $pool.StartMode = "AlwaysRunning"
        $pool.ProcessModel.IdentityType = "ApplicationPoolIdentity"
        $iis.CommitChanges()

        Write-Host "Creating IIS Site... [$siteName]"
        if ($null -ne $iis.Sites -And $null -ne $iis.Sites[$siteName]) {
            $iis.Sites.Remove($iis.Sites[$siteName])    
        }
        $site = $iis.Sites.Add($siteName, "http", "*:80:$($bindings[0])", $iisPath)
        $site.Applications["/"].ApplicationPoolName = $siteName
        $iis.CommitChanges()
        $site = $iis.Sites[$siteName]
        Write-Host "    " adding binding for $bindings[0]
        if($bindings.Length -gt 1) {
            For ($i=1; $i -lt $bindings.Length; $i++) {
                Write-Host "    " adding binding for $bindings[$i]
                $site.Bindings.Add("*:80:$($bindings[$i])", "http")   | Out-Null
            }
        }
        $iis.CommitChanges() | Out-Null
    }

}

function Create-WebConfig {

            begin {
                Write-Host "Creating Web.Config..."
                Import-Module XmlTransform.psm1 -Force
            }

            process {
		        #Transform-Xml .\Config\web.config .\Config\web.$WebConfigVersion.config .\CMS\web.config | Write-Verbose
            }


}

function Update-HostFile {

    begin {
        $bindings = $config.dev.bindings
    }

    process {
        Write-Host "Setting up host file entries..."
        . (Join-Path $PSScriptRoot HostFileUtils.ps1) add 127.0.0.1 $bindings[0]
        Write-Host "    " adding host file entry for $bindings[0]

        if($bindings.Length -gt 1) {
		    For ($i=1; $i -lt $bindings.Length; $i++) {
                Write-Host "    " adding host file entry for $bindings[$i]
			   . (Join-Path $PSScriptRoot HostFileUtils.ps1) add 127.0.0.1 $bindings[$i]
		    }
        }
    }

    end {}
}

function Restore-CIData {

    begin {
        Import-Module $IgnitionPath\PowerShellModules\KenticoCI.psm1
    }

    process {

        $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Restores the Kentico CI data from disk"
        $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Continues without restoring CI data"
        $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
        $result = Show-Menu 'Restore CI Data?' 'Would you like to restore Kentico CI data?' $options

        switch ($result)
        {
            0 {
                try {
                    Restore-KenticoCIData $SolutionPath $IgnitionPath
                } catch {
                    Write-Warning("Kentico CI Restore failed!!")
                }
            }
        }

        
    }

    end {}
}

function Get-MSBuildPath ([switch] $amd64) {
    Write-Host "Finding MSBuild...."
    $vsInstallPath = (Get-VsSetupInstance).InstallationPath    
    $msBuildPaths = Get-ChildItem -Path $vsInstallPath -Recurse | Where-Object { $_.Name -ieq 'MsBuild.exe' }   
    $msBuildPathsSorted = $msBuildPaths | Sort-Object -Property FullName -Descending

    $msBuildPath = $null 
    if ($amd64)
    {
        $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ieq 'amd64' } | Select-Object -ExpandProperty FullName -First 1
    }
    else
    {
        $msBuildPath = $msBuildPathsSorted | Where-Object { $_.Directory.Name -ine 'amd64' } | Select-Object -ExpandProperty FullName -First 1        
    }

    $msBuildPath
}

function Invoke-MSBuild ($solution, $target = "Rebuild", [switch] $amd64) {
    Write-Host ""
    Write-Host "Building Solution..."
    # allow for wildcard matching on $solution
    $solutionMatches = @(Get-ChildItem $solution -ErrorAction SilentlyContinue)
 
    if ($solutionMatches.Length -ne 1) {
        Write-Warning "Expected 1 solution match but found $($solutionMatches.Length). Refine solution path."
        return
    }
 
    $sln = $solutionMatches[0]
    $msBuildPath = Get-MSBuildPath -amd64:$amd64
    
    $UTILS_DIR = ("$env:LOCALAPPDATA\.gyro\igniteSiteUtils\")
    $NUGET = Join-Path $UTILS_DIR "nuget.exe"

    Write-Host "Restoring packages for $($sln.FullName)"
    Invoke-Expression "&`"$NUGET`" restore `"$($sln.FullName)`" -Verbosity quiet"
       
    Write-Host "Running target $($target) for $($sln.FullName)"
    &$msBuildPath -v:q -m $sln.FullName /t:$target  
}

function Show-Menu {
    param(
        [string] $title,
        [string] $message,
        [System.Management.Automation.Host.ChoiceDescription[]] $options
    )

    $result = $host.ui.PromptForChoice($title, $message, $options, 0)

    $result
}

Export-ModuleMember -function Install-Site