#Requires -RunAsAdministrator
[cmdletbinding()]
param(
    [string] $SolutionPath = "",
    [string] $IgnitionPath = ""
)

if ((!$SolutionPath) -or ($SolutionPath -and !(Test-Path $SolutionPath))) {
    $SolutionPath = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
    if (!(Test-Path $SolutionPath)) {
        Throw "Solution path is not found! Please make sure you are executing this command from the correct folder! or check your -SolutionPath parameter"
    }
}

if ((!$IgnitionPath) -or ($IgnitionPath -and !(Test-Path $IgnitionPath))) {
    $IgnitionPath = (Split-Path $MyInvocation.MyCommand.Path -Parent)
    if (!(Test-Path $IgnitionPath)) {
        Throw "Ignition path is not found! Please make sure you are executing this command from the correct folder! or check your -IgnitionPath parameter"
    }
}

#Bootstrap Required Tools
Import-Module $IgnitionPath\PowerShellModules\PreIgnition.psm1 -Force
$UtilsPath = Install-RequiredTools

#Ignite!
Import-Module $IgnitionPath\PowerShellModules\SiteIgnite.psm1 -Force
Install-Site -SolutionPath $SolutionPath -IgnitionPath $IgnitionPath -UtilsPath $UtilsPath 