[cmdletbinding()]
param(
    [string] $IgnitionPath = ""
)
$commandContent =  (Get-Content $IgnitionPath\PowerShellModules\DriveRemapper.ps1) | Out-String
$command = "& { $commandContent }"
$bytes = [System.Text.Encoding]::Unicode.GetBytes($command)
$encodedCommand = [Convert]::ToBase64String($bytes)
pwsh -NoProfile -Command "& {Start-Process pwsh -ArgumentList '-ExecutionPolicy Bypass -encodedCommand $encodedCommand' -Verb RunAs}"