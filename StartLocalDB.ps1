[cmdletbinding()]
param(
    [string] $SolutionPath = "",
    [string] $IgnitionPath = ""
)

if ((!$SolutionPath) -or ($SolutionPath -and !(Test-Path $SolutionPath))) {
    $SolutionPath = Split-Path (Split-Path $MyInvocation.MyCommand.Path -Parent) -Parent
    if (!(Test-Path $SolutionPath)) {
        Throw "Solution path is not found! Please make sure you are executing this command from the correct folder! or check your -SolutionPath parameter"
    }
}

if ((!$IgnitionPath) -or ($IgnitionPath -and !(Test-Path $IgnitionPath))) {
    $IgnitionPath = (Split-Path $MyInvocation.MyCommand.Path -Parent)
    if (!(Test-Path $IgnitionPath)) {
        Throw "Ignition path is not found! Please make sure you are executing this command from the correct folder! or check your -IgnitionPath parameter"
    }
}

Set-Location $SolutionPath
$config = (Get-Content .\ignition.json) | ConvertFrom-Json    
Import-Module $IgnitionPath\PowerShellModules\SQLLocalDBUtils.psm1
$ldb = $config.dev.databaseInstance
Write-Host "Starting (localdb)\$ldb"
Start-LocalDb($config.dev.databaseInstance) | Out-Null